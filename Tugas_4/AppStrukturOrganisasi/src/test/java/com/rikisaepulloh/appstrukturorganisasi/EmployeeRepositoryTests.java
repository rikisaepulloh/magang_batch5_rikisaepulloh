package com.rikisaepulloh.appstrukturorganisasi;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class EmployeeRepositoryTests {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void testAddNew(){
        Employee employee = new Employee();
        employee.setNama("Aly Rachman");
        employee.setJabatan("Direktur");
        employee.setAtasanId("Riki Saepulloh");
        employee.setCompany("PT JAVAN");

        Employee savedUser = employeeRepository.save(employee);
        Assertions.assertThat(savedUser).isNotNull();
        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }
    @Test
    public void testListAll(){
        Iterable<Employee> employees = employeeRepository.findAll();
        Assertions.assertThat(employees).hasSizeGreaterThan(0);

        for (Employee employee : employees) {
            System.out.print(employee);
        }
    }
    @Test
    public void testUpdate(){
        long employeeId = 1;
        Optional<Employee> optionalEmployee = employeeRepository.findAllById(employeeId);
        Employee employee = optionalEmployee.get();
        employee.setCompany("PT DICODE");
        employeeRepository.save(employee);

        Employee updateEmployee = employeeRepository.findAllById(employeeId).get();
        Assertions.assertThat(updateEmployee.getCompany()).isEqualTo("PT DICODE");

    }
    @Test
    public void testGet(){
        long employeeId = 2;
        Optional<Employee> optionalEmployee = employeeRepository.findAllById(employeeId);
        Assertions.assertThat(optionalEmployee).isPresent();
        System.out.print(optionalEmployee.get());
    }
    @Test
    public void testDelete(){
        long employeeId = 2;
        employeeRepository.deleteById(employeeId);

        Optional<Employee> optionalEmployee = employeeRepository.findById(employeeId);
        Assertions.assertThat(optionalEmployee).isNotPresent();
    }
}
