package com.rikisaepulloh.appstrukturorganisasi;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @GetMapping("/employee")
    public String showUserList(Model model){
        List<Employee> employeeList = service.listAll();
        model.addAttribute("employeeList", employeeList);

        return "employee";
    }

    @GetMapping("/employee/new")
    public String showNewForm(Model model){
        model.addAttribute("employee", new Employee());
        model.addAttribute("pageTitle", "Add new Employee");
        return "employee_form";
    }

    @PostMapping("/employee/save")
    public String saveEmployee(Employee employee, RedirectAttributes ra){
        service.save(employee);
        ra.addAttribute("message","The employee has been saved successfully");

        return "redirect:/employee";
    }

    @GetMapping("/employee/edit/{id}")
    public String showEditForm(@PathVariable("id") long id, Model model, RedirectAttributes ra){
        try{
            Employee employee = service.get(id);
            model.addAttribute("employee", employee);
            model.addAttribute("pageTitle", "Edit Employee (ID: " + id + ")");
            return "employee_form";
        }catch (NotFoundException e){
            ra.addAttribute("message", e.getMessage());
            return "redirect:/employee";
        }
    }
    @GetMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable("id") long id, RedirectAttributes ra){
        try{
            service.delete(id);
        }catch (NotFoundException e){
            ra.addAttribute("message", e.getMessage());
        }
        return "redirect:/employee";
    }
    @GetMapping("/export/pdf")
    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=file.pdf";
        response.setHeader(headerKey, headerValue);
        Map<String, Object> map;
        List<Employee> emp = service.listAll();
        String[] tblHeader = {"ID", "Nama", "Jabatan", "Perusahaan"};
        List<Map<String, Object>> maps = new ArrayList<>();
        for (Employee employee : emp) {
            map = new HashMap<>();
            map.put("id", employee.getId());
            map.put("nama", employee.getNama());
            map.put("jabatan", employee.getJabatan());
            map.put("company", employee.getCompany());
            maps.add(map);
        }
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        Paragraph p = new Paragraph("Data Employee");
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
        PdfPTable table = new PdfPTable(tblHeader.length);
        table.setWidthPercentage(100f);
        table.setWidths(new float[]{1f, 3.0f, 3.0f, 3.0f});
        table.setSpacingBefore(10);
        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        for (String str : tblHeader) {
            cell.setPhrase(new Phrase(str));
            table.addCell(cell);
        }
        for (Map<String, Object> map1 : maps) {
            table.addCell(String.valueOf(map1.get("id")));
            table.addCell(String.valueOf(map1.get("nama")));
            table.addCell(String.valueOf(map1.get("jabatan")));
            table.addCell(String.valueOf(map1.get("company")));
        }
        document.add(table);
        document.close();
    }
    @GetMapping("/export/excel")
    public ResponseEntity<InputStreamResource> exportToExcel(HttpServletResponse response) {
        ByteArrayInputStream byteArrayInputStream = null;
        String[] tblHeader = {"ID", "Nama", "Jabatan", "Company"};
        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            CreationHelper creationHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("dataPengajar");
            org.apache.poi.ss.usermodel.Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(0);
            for (int i = 0; i < tblHeader.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(tblHeader[i]);
                cell.setCellStyle(headerCellStyle);
            }
            int rowIdx = 1;
            Map<String, Object> map = new HashMap<>();
            List<Employee> emp = service.listAll();
            List<Map<String, Object>> maps = new ArrayList<>();
            for (Employee employee : emp) {
                map = new HashMap<>();
                map.put("id", employee.getId());
                map.put("nama", employee.getNama());
                map.put("jabatan", employee.getJabatan());
                map.put("company", employee.getCompany());
                maps.add(map);
            }
            for (Map<String, Object> map1 : maps) {
                Row row = sheet.createRow(rowIdx);
                Set<String> stringSet = map.keySet();
                row.createCell(0).setCellValue(String.valueOf(map1.get("id")));
                row.createCell(1).setCellValue(String.valueOf(map1.get("nama")));
                row.createCell(2).setCellValue(String.valueOf(map1.get("jabatan")));
                row.createCell(3).setCellValue(String.valueOf(map1.get("company")));
                rowIdx++;
            }
            workbook.write(out);
            workbook.close();
            byteArrayInputStream = new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            System.out.println(e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=file.xlsx");
        return ResponseEntity.ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new InputStreamResource(byteArrayInputStream));
    }
}
