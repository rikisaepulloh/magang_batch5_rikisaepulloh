package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class CompanyController {
    @Autowired
    private CompanyService service;

    @GetMapping("/company")
    public String showUserList(Model model){
        List<Company> companyList = service.listAll();
        model.addAttribute("companyList", companyList);

        return "company";
    }

    @GetMapping("/company/new")
    public String showNewForm(Model model){
        model.addAttribute("company", new Company());
        model.addAttribute("pageTitle", "Add new Company");
        return "company_form";
    }

    @PostMapping("/company/save")
    public String saveEmployee(Company company, RedirectAttributes ra){
        service.save(company);
        ra.addAttribute("message","The employee has been saved successfully");

        return "redirect:/company";
    }

    @GetMapping("/company/edit/{id}")
    public String showEditForm(@PathVariable("id") long id, Model model, RedirectAttributes ra){
        try{
            Company company = service.get(id);
            model.addAttribute("company", company);
            model.addAttribute("pageTitle", "Edit Company (ID: " + id + ")");
            return "company_form";
        }catch (NotFoundException e){
            ra.addAttribute("message", e.getMessage());
            return "redirect:/company";
        }
    }
    @GetMapping("/company/delete/{id}")
    public String deleteEmployee(@PathVariable("id") long id, RedirectAttributes ra){
        try{
            service.delete(id);
        }catch (NotFoundException e){
            ra.addAttribute("message", e.getMessage());
        }
        return "redirect:/company";
    }
}
