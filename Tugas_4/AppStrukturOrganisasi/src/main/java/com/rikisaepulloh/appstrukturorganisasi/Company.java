package com.rikisaepulloh.appstrukturorganisasi;

import javax.persistence.*;

@Entity
@Table(name = "data_company")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String namaCompany;
    private String alamat;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamaCompany() {
        return namaCompany;
    }

    public void setNamaCompany(String company) {
        this.namaCompany = company;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", namaCompany='" + namaCompany + '\'' +
                ", alamat='" + alamat + '\'' +
                '}';
    }
}
