package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Optional<Employee> findAllById(long employeeId);
    public Long countById(long id);
}
