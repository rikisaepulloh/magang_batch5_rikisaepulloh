package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CompanyRepository extends CrudRepository<Company, Long> {
    Optional<Company> findAllById(long companyId);
    public Long countById(long id);
}
