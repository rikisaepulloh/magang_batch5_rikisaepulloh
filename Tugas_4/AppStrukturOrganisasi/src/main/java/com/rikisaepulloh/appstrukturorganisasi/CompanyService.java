package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> listAll(){
        return (List<Company>) companyRepository.findAll();
    }
    public void save(Company company){
        companyRepository.save(company);
    }
    public Company get(long id) throws NotFoundException {
        Optional<Company> result = companyRepository.findById(id);
        if(result.isPresent()){
            return result.get();
        }
        throw new NotFoundException("Could not find any employee with Id" + id);
    }
    public void delete(long id) throws NotFoundException {
        Long count = companyRepository.countById(id);
        if(count == null || count ==0){
            throw new NotFoundException("Could not find any employee with Id" + id);
        }
        companyRepository.deleteById(id);
    }
}
