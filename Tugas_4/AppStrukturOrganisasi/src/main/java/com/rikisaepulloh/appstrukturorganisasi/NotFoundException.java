package com.rikisaepulloh.appstrukturorganisasi;

public class NotFoundException extends Throwable {
    public NotFoundException(String message) {
        super(message);
    }
}
