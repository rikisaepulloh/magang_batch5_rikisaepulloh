package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> listAll(){
        return (List<Employee>) employeeRepository.findAll();
    }
    public void save(Employee employee){
        employeeRepository.save(employee);
    }
    public Employee get(long id) throws NotFoundException {
        Optional<Employee> result = employeeRepository.findById(id);
        if(result.isPresent()){
            return result.get();
        }
        throw new NotFoundException("Could not find any employee with Id" + id);
    }
    public void delete(long id) throws NotFoundException {
        Long count = employeeRepository.countById(id);
        if(count == null || count ==0){
            throw new NotFoundException("Could not find any employee with Id" + id);
        }
        employeeRepository.deleteById(id);
    }
}