package com.company;

import java.util.*;

public class test {
    public static int Box(int row, int col) {
        row++;
        col++;

        if (row >= 1 && row <= 3) {
            if (col >= 1 && col <= 3) {
                return 1;
            }
            if (col >= 4 && col <= 6) {
                return 2;
            }
            if (col >= 7 && col <= 9) {
                return 3;
            }
        }
        if (row >= 4 && row <= 6) {
            if (col >= 1 && col <= 3) {
                return 4;
            }
            if (col >= 4 && col <= 6) {
                return 5;
            }
            if (col >= 7 && col <= 9) {
                return 6;
            }
        }
        if (row >= 7 && row <= 9) {
            if (col >= 1 && col <= 3) {
                return 7;
            }
            if (col >= 4 && col <= 6) {
                return 8;
            }
            if (col >= 7 && col <= 9) {
                return 9;
            }
        }
        return 0;
    }

    public static String sudokuChecker(String[] arr) {
        int[][] sudoku = new int[9][9];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length() - 1; j++) {
                if (j % 2 == 0 || j == 0) {
                    sudoku[i][j / 2] = Integer.parseInt(arr[i].substring(j + 1, j + 2));
                }
            }
        }

        // Variable Error
        List<Integer> error = new ArrayList<Integer>();

        List<String> errorBox = new ArrayList<String>(); // Opsional
        List<String> errorRow = new ArrayList<String>(); // Opsional
        List<String> errorCol = new ArrayList<String>(); // Opsional

        // Cek Duplikat Angka di Dalam Box
        //Pengulangan Iterasi 3x
        for (int sec = 0; sec < 3; sec++) {
            for (int i = 0; i < sudoku.length; i++) {
                // Cek Angka per Box
                for (int j = 0 + sec * 3; j < 3 + sec * 3; j++) {
                    if (sudoku[i][j] != 0) {
                        for (int k = j; k < 3 + sec * 3; k++) {
                            if (sudoku[i][j] == sudoku[i][k] && k != j) {
                                if (!error.contains(Box(i, j))) {
                                    error.add(Box(i, j));
                                }
                                if (!error.contains(Box(i, k))) {
                                    error.add(Box(i, k));
                                }
                                errorBox.add("" + Box(i, j) + " " + Box(i, k));
                            }
                            // Menghindari Error Array Out Bound
                            if (i + 1 < sudoku.length) {
                                if (sudoku[i][j] == sudoku[i + 1][k]) {
                                    if (!error.contains(Box(i, j))) {
                                        error.add(Box(i, j));
                                    }
                                    if (!error.contains(Box(i, k))) {
                                        error.add(Box(i, k));
                                    }
                                    errorBox.add("" + Box(i, j) + " " + Box(i, k));
                                }
                            }
                            // Menghindari Error Array Out Bound
                            if (i + 2 < sudoku.length) {
                                if (sudoku[i][j] == sudoku[i + 2][k]) {
                                    if (!error.contains(Box(i, j))) {
                                        error.add(Box(i, j));
                                    }
                                    if (!error.contains(Box(i, k))) {
                                        error.add(Box(i, k));
                                    }
                                    errorBox.add("" + Box(i, j) + " " + Box(i, k));
                                }
                            }
                        }
                    }
                }
            }
        }

        //Cek Duplikat Angka di Setiap Baris
        for (int i = 0; i < sudoku.length; i++) {

            // Cek Angka per Box
            for (int j = 0; j < sudoku[i].length; j++) {
                if (sudoku[i][j] != 0) {
                    for (int k = j + 1; k < sudoku[i].length; k++) {
                        if (sudoku[i][j] == sudoku[i][k]) {
                            if (!error.contains(Box(i, j))) {
                                error.add(Box(i, j));
                            }
                            if (!error.contains(Box(i, k))) {
                                error.add(Box(i, k));
                            }
                            errorRow.add("" + Box(i, j) + " " + Box(i, k));
                        }
                    }
                }
            }
        }

        //Cek Duplikat Angka di Setiap Kolom
        for (int i = 0; i < sudoku.length; i++) {

            // Cek Angka per Box
            for (int j = 0; j < sudoku[i].length; j++) {
                if (sudoku[j][i] != 0) {
                    for (int k = j + 1; k < sudoku[i].length; k++) {
                        if (sudoku[j][i] == sudoku[k][i]) {
                            if (!error.contains(Box(j, i))) {
                                error.add(Box(j, i));
                            }
                            if (!error.contains(Box(k, i))) {
                                error.add(Box(k, i));
                            }
                            errorCol.add("" + Box(j, i) + " " + Box(k, i));
                        }
                    }
                }
            }
        }

        // Final Output
        if (!error.isEmpty()) {
            Collections.sort(error); //Mengurutkan Angka box
        }
        return "legal";
    }
    public static void main (String[]args){
        // keep this function call here
        Scanner s = new Scanner(System.in);
        System.out.print(sudokuChecker(new String[]{"(1,2,3,4,5,6,7,8,1)",
                "(0,1,0,0,0,0,0,0,0)",
                "(0,0,2,2,0,0,0,0,0)",
                "(3,2,2,0,0,0,0,0,0)",
                "(2,0,0,0,0,0,0,0,0)",
                "(0,0,0,0,0,0,0,0,0)",
                "(0,0,0,0,0,0,0,0,0)",
                "(0,0,0,0,0,0,0,8,8)",
                "(0,0,0,0,0,0,0,0,0)"}));
    }
}