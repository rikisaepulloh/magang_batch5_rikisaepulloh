package com.company;

import java.lang.reflect.Array;
import java.util.Scanner;

class calkulator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan Angka : ");
        String inputUser = input.nextLine();
        String[] bedahAngka = inputUser.split(" ");
        final int firstNumber = Integer.parseInt(String.valueOf(bedahAngka[0]));
        final int secondNumber = Integer.parseInt(String.valueOf(bedahAngka[2]));
        String operasi = bedahAngka[1];
        if (String.valueOf(operasi).equals("x")) {
            int hasilOperasi = firstNumber * secondNumber;
            System.out.print(hasilOperasi);
        }
        else if(String.valueOf(operasi).equals("/")){
            int hasilOperasi = firstNumber / secondNumber;
            System.out.print(hasilOperasi);
        }
        else if(String.valueOf(operasi).equals("+")){
            int hasilOperasi = firstNumber + secondNumber;
            System.out.print(hasilOperasi);
        }
        else if(String.valueOf(operasi).equals("-")){
            int hasilOperasi = firstNumber - secondNumber;
            System.out.print(hasilOperasi);
        }
        else{
            System.out.print("Error!!");
        }
    }

}