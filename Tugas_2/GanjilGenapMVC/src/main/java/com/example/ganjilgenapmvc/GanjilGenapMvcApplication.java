package com.example.ganjilgenapmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class GanjilGenapMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(GanjilGenapMvcApplication.class, args);
    }

}
