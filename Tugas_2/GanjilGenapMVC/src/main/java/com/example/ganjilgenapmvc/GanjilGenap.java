package com.example.ganjilgenapmvc;

import java.util.ArrayList;
import java.util.List;

public class GanjilGenap {
    public List<String> tukar(int a, int b) {
        List<String> ls = new ArrayList<>();
        for (int i = a; i <= b; i++) {
            String str = "ganjil";
            if (i % 2 == 0) {
                str = "genap";
            }
            ls.add("Angka " + i + " adalah " + str);
        }
        return ls;
    }

}
