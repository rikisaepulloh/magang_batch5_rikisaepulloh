package com.example.ganjilgenapmvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.nio.file.Paths.get;

@Controller
public class ControllerGanjilGenap {
    private GanjilGenap ganjilGenap;
    @GetMapping("/")
    public String ganjilGenap(Map<String, Object> model, @RequestBody(required = false) String par) throws MalformedURLException {
        model.put("title", "Ganjil Genap");
        List<String> res = new ArrayList<>();
        if (par != null) {
            model.put("a", get("a").toString());
            model.put("b", get("b").toString());
            res = ganjilGenap.tukar(Integer.parseInt(get("a").toString()), Integer.parseInt(get("b").toString()));
        }
        model.put("res", res);
        return "index";
    }


}
