package com.example.ganjilgenapmvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class Rest {
    private GanjilGenap ganjilGenap;
    @PostMapping("/")
    public List<String> ganjilGenap(@RequestBody Map<String, Object> par) {
        return ganjilGenap.tukar(Integer.parseInt(par.get("a").toString()), Integer.parseInt(par.get("b").toString()));
    }

}
