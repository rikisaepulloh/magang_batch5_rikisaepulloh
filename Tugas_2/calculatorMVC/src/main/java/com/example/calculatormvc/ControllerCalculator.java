package com.example.calculatormvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ControllerCalculator {
    @GetMapping("/")
    public String main(@RequestParam(name = "angka1", required = false,defaultValue = "0") int angka1,
                       @RequestParam(name = "angka2",required = false, defaultValue = "0")int angka2,
                       @RequestParam(name = "operator",required = false, defaultValue = "")String operator, Model model){
        model.addAttribute("angka1",angka1);
        model.addAttribute("operator",operator);
        model.addAttribute("angka2",angka2);
        int hasil = 0;
        if(operator.equals("+")){
            hasil = angka1 + angka2;
        }
        else if(operator.equals("-")){
            hasil = angka1 - angka2;
        }
        else if(operator.equals("x")){
            hasil = angka1 * angka2;
        }
        else if(operator.equals("/")){
            hasil = angka1 / angka2;
        }
        model.addAttribute("hasil", hasil);
        return "index";
    }
}
