package com.example.hitungvocalmvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ControllerHitungVocal {
    @GetMapping("/")
    public String main(@RequestParam(name = "kata", required = false, defaultValue = "") String kata,
                       Model model) {

        model.addAttribute("kata",kata);
        String hasil1= "";
        String hasil2= "";
        int[] jum = new int[256];

        for (int i = 0; i < 256; i++) {
            jum[i] = 0;
        }

        int jumlahVokal = 0;
        for (int i = 0; i < kata.length(); i++) {
            jum[(int) kata.charAt(i)]++;
            if (kata.charAt(i) == 'a' || kata.charAt(i) == 'i' || kata.charAt(i) == 'u' || kata.charAt(i) == 'e' || kata.charAt(i) == 'o') {
                jumlahVokal++;
            }
        }

        hasil1 = kata + " = " + jumlahVokal + " kata yaitu terdiri dari : ";

        for (int i = 0; i < 256; i++) {
            if (jum[i] > 0) {
                if ((char) i == 'a' || (char) i == 'i' || (char) i == 'u' || (char) i == 'e' || (char) i == 'o') {
                    hasil2 = "Jumlah huruf " + (char) i + " adalah " + jum[i];
                }
            }
        }

        model.addAttribute("hasil1",hasil1);
        model.addAttribute("hasil2",hasil2);
        return "index";
    }
}
