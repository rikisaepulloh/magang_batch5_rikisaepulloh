package com.example.hitungvocalmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HitungVocalMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(HitungVocalMvcApplication.class, args);
    }

}
