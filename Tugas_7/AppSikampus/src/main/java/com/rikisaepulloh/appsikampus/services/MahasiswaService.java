package com.rikisaepulloh.appsikampus.services;

import com.rikisaepulloh.appsikampus.pojos.Mahasiswa;

public interface MahasiswaService extends BaseService<Mahasiswa> {
}
