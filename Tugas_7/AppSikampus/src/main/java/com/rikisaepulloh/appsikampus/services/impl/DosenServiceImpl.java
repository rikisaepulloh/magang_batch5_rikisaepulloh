package com.rikisaepulloh.appsikampus.services.impl;

import com.rikisaepulloh.appsikampus.models.DosenRepository;
import com.rikisaepulloh.appsikampus.models.RiwayatPendidikanRepository;
import com.rikisaepulloh.appsikampus.pojos.Dosen;
import com.rikisaepulloh.appsikampus.pojos.RiwayatPendidikan;
import com.rikisaepulloh.appsikampus.services.DosenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Service
public class DosenServiceImpl implements DosenService {

    @Autowired
    private DosenRepository dosenRepository;
    @Autowired
    private RiwayatPendidikanRepository riwayatPendidikanRepository;

    @Override
    public Page<Dosen> findDataAll(Map<String, Object> par) {
        String parSort = par.get("sort") == null ? "idDosen" : par.get("sort").toString();
        String order = par.get("order") == null ? "asc" : par.get("order").toString();
        int pg = par.get("page") == null ? 0 : Integer.parseInt(par.get("page").toString()) - 1;
        int size = par.get("size") == null ? 10 : Integer.parseInt(par.get("size").toString());
        Sort sort = Sort.by(parSort);
        if (order.equals("asc")) {
            sort = sort.ascending();
        } else {
            sort = sort.descending();
        }
        Pageable pageable = PageRequest.of(pg, size, sort);
        Page<Dosen> page = null;
        if (par.get("keyword") == null) {
            page = dosenRepository.findAll(pageable);
        } else {
            pageable = PageRequest.of(pg, size, sort);
            page = dosenRepository.findAll(new Specification<Dosen>() {
                @Override
                public Predicate toPredicate(Root<Dosen> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(criteriaBuilder.like(root.get("nmDosen"), "%" + par.get("keyword").toString() + "%"));
                    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
            }, pageable);
        }
        return page;
    }

    @Override
    public Optional<Dosen> findDataById(long id) {
        return dosenRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Dosen par) {
        Map<String, Object> map = new HashMap<>();
        RiwayatPendidikan pendidikan = new RiwayatPendidikan();
        List<Long> idRiwayats = new ArrayList<>();
        List<RiwayatPendidikan> riwayatPendidikans = new ArrayList<>();
        if (par.getIdDosen() == 0) {
            long id = dosenRepository.getMaxId() + 1;
            par.setIdDosen(id);
            for (int i = 0; i < par.getRiwayatPendidikans().size(); i++) {
                par.getRiwayatPendidikans().get(i).setDosen(new Dosen());
                par.getRiwayatPendidikans().get(i).getDosen().setIdDosen(id);
            }
        } else {
            for (int i = 0; i < par.getRiwayatPendidikans().size(); i++) {
                par.getRiwayatPendidikans().get(i).setDosen(new Dosen());
                par.getRiwayatPendidikans().get(i).getDosen().setIdDosen(par.getIdDosen());
                if (par.getRiwayatPendidikans().get(i).getIdRiwayat() == 0) {
                    if (par.getRiwayatPendidikans().get(i).getJurusan() != null) {
                        riwayatPendidikans.add(par.getRiwayatPendidikans().get(i));
                    }
                } else {
                    idRiwayats.add(par.getRiwayatPendidikans().get(i).getIdRiwayat());
                }
            }
        }
        Dosen dosen = dosenRepository.save(par);
        if (dosen.getIdDosen() > 0) {
            riwayatPendidikans = riwayatPendidikanRepository.saveAll(riwayatPendidikans);
            for (int i = 0; i < riwayatPendidikans.size(); i++) {
                idRiwayats.add(riwayatPendidikans.get(i).getIdRiwayat());
            }
            riwayatPendidikans = riwayatPendidikanRepository.findAllByDosenIdDosenAndIdRiwayatNotIn(par.getIdDosen(), idRiwayats);
            if (riwayatPendidikans.size() > 0) {
                riwayatPendidikanRepository.deleteAll(riwayatPendidikans);
            }
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Dosen par) {
        Map<String, Object> map = new HashMap<>();
        dosenRepository.delete(par);
        long cn = dosenRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }

    @Override
    public Map<String, Object> saveDataMatakuliah(Dosen par) {
        Map<String, Object> map = new HashMap<>();
        Dosen dosen = dosenRepository.save(par);
        if (dosen.getIdDosen() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }
}
