package com.rikisaepulloh.appsikampus.services.impl;

import com.rikisaepulloh.appsikampus.models.MahasiswaRepository;
import com.rikisaepulloh.appsikampus.pojos.Mahasiswa;
import com.rikisaepulloh.appsikampus.services.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Page<Mahasiswa> findDataAll(Map<String, Object> par) {
        String parSort = par.get("sort") == null ? "idMahasiswa" : par.get("sort").toString();
        String order = par.get("order") == null ? "asc" : par.get("order").toString();
        int pg = par.get("page") == null ? 0 : Integer.parseInt(par.get("page").toString()) - 1;
        int size = par.get("size") == null ? 10 : Integer.parseInt(par.get("size").toString());
        Sort sort = Sort.by(parSort);
        if (order.equals("asc")) {
            sort = sort.ascending();
        } else {
            sort = sort.descending();
        }
        Pageable pageable = PageRequest.of(pg, size, sort);
        Page<Mahasiswa> page = null;
        if (par.get("keyword") == null) {
            page = mahasiswaRepository.findAll(pageable);
        } else {
            pageable = PageRequest.of(pg, size, sort);
            page = mahasiswaRepository.findAll(new Specification<Mahasiswa>() {
                @Override
                public Predicate toPredicate(Root<Mahasiswa> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(criteriaBuilder.or(
                            criteriaBuilder.like(root.get("nim"), "%" + par.get("keyword").toString() + "%"),
                            criteriaBuilder.like(root.get("namaMahasiswa"), "%" + par.get("keyword").toString() + "%"))
                    );
                    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
            }, pageable);
        }
        return page;
    }

    @Override
    public Optional<Mahasiswa> findDataById(long id) {
        return mahasiswaRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Mahasiswa par) {
        Map<String, Object> map = new HashMap<>();
        Mahasiswa mahasiswa = mahasiswaRepository.save(par);
        if (mahasiswa.getIdMahasiswa() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Mahasiswa par) {
        Map<String, Object> map = new HashMap<>();
        mahasiswaRepository.delete(par);
        long cn = mahasiswaRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}
