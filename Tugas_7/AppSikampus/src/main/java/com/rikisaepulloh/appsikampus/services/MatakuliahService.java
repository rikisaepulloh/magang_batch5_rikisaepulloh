package com.rikisaepulloh.appsikampus.services;

import com.rikisaepulloh.appsikampus.pojos.Matakuliah;

public interface MatakuliahService extends BaseService<Matakuliah> {
}
