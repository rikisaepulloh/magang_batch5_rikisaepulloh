package com.rikisaepulloh.appsikampus.controllers;

import com.rikisaepulloh.appsikampus.pojos.Kelas;
import com.rikisaepulloh.appsikampus.pojos.Mahasiswa;
import com.rikisaepulloh.appsikampus.pojos.Matakuliah;
import com.rikisaepulloh.appsikampus.services.KelasService;
import com.rikisaepulloh.appsikampus.services.MahasiswaService;
import com.rikisaepulloh.appsikampus.services.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/kelas")
public class KelasController {

    @Autowired
    @Qualifier("kelasServiceImpl")
    private KelasService kelasService;
    @Autowired
    @Qualifier("matakuliahServiceImpl")
    private MatakuliahService matakuliahService;
    @Autowired
    @Qualifier("mahasiswaServiceImpl")
    private MahasiswaService mahasiswaService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        map.put("size", 200);
        Page<Matakuliah> matakuliahs = matakuliahService.findDataAll(map);
        mav.addObject("data", matakuliahs);
        mav.addObject("mainPg", "kelas/list");
        return mav;
    }

    @GetMapping(path = "/mahasiswa")
    public ModelAndView viewMatkulMahasiswa(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        mav.addObject("mainPg", "mahasiswa/kelas");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Mahasiswa mahasiswa = new Mahasiswa();
        mav.addObject("mahasiswa", mahasiswa);
        if (id > 0) {
            Optional<Mahasiswa> optionalMahasiswa = mahasiswaService.findDataById(id);
            if (optionalMahasiswa.isPresent()) {
                mav.addObject("mahasiswa", optionalMahasiswa.get());
            }
        }
        mav.addObject("mainPg", "kelas/kelas");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Mahasiswa par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = kelasService.saveData(par.getKelass());
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/kelas");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Kelas par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = kelasService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/kelas";
    }

}
