package com.rikisaepulloh.appsikampus.models;

import com.rikisaepulloh.appsikampus.pojos.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MatakuliahRepository extends JpaRepository<Matakuliah, Long>, JpaSpecificationExecutor<Matakuliah> {

}
