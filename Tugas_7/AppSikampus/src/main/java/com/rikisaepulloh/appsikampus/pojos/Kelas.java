package com.rikisaepulloh.appsikampus.pojos;

import com.rikisaepulloh.appsikampus.pojos.embeds.MahasiswaMatakuliahIdentity;


import javax.persistence.*;

@Entity
@Table(name = "kelas", schema = "si_kampus")

public class Kelas {

    @EmbeddedId
    private MahasiswaMatakuliahIdentity mahasiswaMatakuliahIdentity;

    @Column(columnDefinition = "varchar(50)")
    private String namaKelas;

    @MapsId("idMahasiswa")
    @ManyToOne
    private Mahasiswa mahasiswa;
    @MapsId("idMatakuliah")
    @ManyToOne
    private Matakuliah matakuliah;

    public MahasiswaMatakuliahIdentity getMahasiswaMatakuliahIdentity() {
        return mahasiswaMatakuliahIdentity;
    }

    public void setMahasiswaMatakuliahIdentity(MahasiswaMatakuliahIdentity mahasiswaMatakuliahIdentity) {
        this.mahasiswaMatakuliahIdentity = mahasiswaMatakuliahIdentity;
    }

    public String getNamaKelas() {
        return namaKelas;
    }

    public void setNamaKelas(String namaKelas) {
        this.namaKelas = namaKelas;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }
}

