package com.rikisaepulloh.appsikampus.pojos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "dosen", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idDosen")

public class Dosen {

    @Id
    @Column(columnDefinition = "int(11)")
    private long idDosen;
    @Column(columnDefinition = "varchar(50)")
    private String namaDosen;
    @Column(columnDefinition = "char(13)", unique = true)
    private String nip;
    @Column(columnDefinition = "varchar(10)")
    private String gelar;

    public long getIdDosen() {
        return idDosen;
    }

    public void setIdDosen(long idDosen) {
        this.idDosen = idDosen;
    }

    public String getNamaDosen() {
        return namaDosen;
    }

    public void setNamaDosen(String namaDosen) {
        this.namaDosen = namaDosen;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getGelar() {
        return gelar;
    }

    public void setGelar(String gelar) {
        this.gelar = gelar;
    }

    public List<RiwayatPendidikan> getRiwayatPendidikans() {
        return riwayatPendidikans;
    }

    public void setRiwayatPendidikans(List<RiwayatPendidikan> riwayatPendidikans) {
        this.riwayatPendidikans = riwayatPendidikans;
    }

    public List<Matakuliah> getMatakuliahs() {
        return matakuliahs;
    }

    public void setMatakuliahs(List<Matakuliah> matakuliahs) {
        this.matakuliahs = matakuliahs;
    }

    @OneToMany(mappedBy = "dosen", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = RiwayatPendidikan.class)
    private List<RiwayatPendidikan> riwayatPendidikans;

    @ManyToMany(targetEntity = Matakuliah.class, mappedBy = "dosens", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private List<Matakuliah> matakuliahs;


}
