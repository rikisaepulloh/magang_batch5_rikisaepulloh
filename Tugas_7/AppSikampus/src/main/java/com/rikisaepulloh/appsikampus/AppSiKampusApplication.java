package com.rikisaepulloh.appsikampus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppSiKampusApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSiKampusApplication.class, args);
    }

}
