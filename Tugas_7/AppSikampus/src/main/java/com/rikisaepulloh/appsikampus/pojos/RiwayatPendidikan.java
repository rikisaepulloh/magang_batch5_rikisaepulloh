package com.rikisaepulloh.appsikampus.pojos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "riwayat_pendidikan", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idRiwayat")
public class RiwayatPendidikan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long idRiwayat;
    @Column(columnDefinition = "varchar(10)")
    private String strata;
    @Column(columnDefinition = "varchar(50)")
    private String jurusan;
    @Column(columnDefinition = "varchar(50)")
    private String sekolah;
    @Column(columnDefinition = "char(5)")
    private String tahunMulai;
    @Column(columnDefinition = "char(5)")
    private String tahunSelesai;

    public long getIdRiwayat() {
        return idRiwayat;
    }

    public void setIdRiwayat(long idRiwayat) {
        this.idRiwayat = idRiwayat;
    }

    public String getStrata() {
        return strata;
    }

    public void setStrata(String strata) {
        this.strata = strata;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getSekolah() {
        return sekolah;
    }

    public void setSekolah(String sekolah) {
        this.sekolah = sekolah;
    }

    public String getTahunMulai() {
        return tahunMulai;
    }

    public void setTahunMulai(String tahunMulai) {
        this.tahunMulai = tahunMulai;
    }

    public String getTahunSelesai() {
        return tahunSelesai;
    }

    public void setTahunSelesai(String tahunSelesai) {
        this.tahunSelesai = tahunSelesai;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_dosen", referencedColumnName = "idDosen", nullable = true)
    private Dosen dosen;

}
