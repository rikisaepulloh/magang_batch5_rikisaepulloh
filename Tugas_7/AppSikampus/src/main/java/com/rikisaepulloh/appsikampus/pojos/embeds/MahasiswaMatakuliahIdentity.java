package com.rikisaepulloh.appsikampus.pojos.embeds;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import java.io.Serializable;

@Embeddable

public class MahasiswaMatakuliahIdentity implements Serializable {

    @Column(columnDefinition = "int(11)")
    @JoinColumn(name = "id_mahasiswa", referencedColumnName = "idMahasiswa", nullable = true)
    private long idMahasiswa;
    @Column(columnDefinition = "int(11)")
    @JoinColumn(name = "id_matakuliah", referencedColumnName = "idMatakuliah", nullable = true)
    private long idMatakuliah;

    public long getIdMahasiswa() {
        return idMahasiswa;
    }

    public void setIdMahasiswa(long idMahasiswa) {
        this.idMahasiswa = idMahasiswa;
    }

    public long getIdMatakuliah() {
        return idMatakuliah;
    }

    public void setIdMatakuliah(long idMatakuliah) {
        this.idMatakuliah = idMatakuliah;
    }
}
