package com.rikisaepulloh.appsikampus.controllers;

import com.rikisaepulloh.appsikampus.pojos.Matakuliah;
import com.rikisaepulloh.appsikampus.services.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/matakuliah")
public class MatakuliahController {

    @Autowired
    @Qualifier("matakuliahServiceImpl")
    private MatakuliahService matakuliahService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        Page<Matakuliah> matakuliahs = matakuliahService.findDataAll(map);
        mav.addObject("data", matakuliahs);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "matakuliah/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Matakuliah matakuliah = new Matakuliah();
        mav.addObject("matakuliah", matakuliah);
        if (id > 0) {
            Optional<Matakuliah> optionalMatakuliah = matakuliahService.findDataById(id);
            if (optionalMatakuliah.isPresent()) {
                mav.addObject("matakuliah", optionalMatakuliah.get());
            }
        }
        mav.addObject("mainPg", "matakuliah/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Matakuliah par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = matakuliahService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/matakuliah");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Matakuliah par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = matakuliahService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/matakuliah";
    }

}
