package com.rikisaepulloh.appsikampus.pojos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "matakuliah", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idMatakuliah")

public class Matakuliah {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long idMatakuliah;
    @Column(columnDefinition = "varchar(50)")
    private String namaMatakuliah;
    @Column(columnDefinition = "int(5)")
    private String jmlSks;

    public long getIdMatakuliah() {
        return idMatakuliah;
    }

    public void setIdMatakuliah(long idMatakuliah) {
        this.idMatakuliah = idMatakuliah;
    }

    public String getNamaMatakuliah() {
        return namaMatakuliah;
    }

    public void setNamaMatakuliah(String namaMatakuliah) {
        this.namaMatakuliah = namaMatakuliah;
    }

    public String getJmlSks() {
        return jmlSks;
    }

    public void setJmlSks(String jmlSks) {
        this.jmlSks = jmlSks;
    }

    public List<Dosen> getDosens() {
        return dosens;
    }

    public void setDosens(List<Dosen> dosens) {
        this.dosens = dosens;
    }

    public List<Kelas> getKelass() {
        return kelass;
    }

    public void setKelass(List<Kelas> kelass) {
        this.kelass = kelass;
    }

    @ManyToMany(targetEntity = Dosen.class, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "dosenMatakuliah",
            joinColumns = @JoinColumn(name = "id_matakuliah", referencedColumnName = "idMatakuliah"),
            inverseJoinColumns = @JoinColumn(name = "id_dosen", referencedColumnName = "idDosen"))
    private List<Dosen> dosens;

    @OneToMany(mappedBy = "mahasiswa", cascade = CascadeType.ALL, targetEntity = Kelas.class)
    private List<Kelas> kelass;

}
