package com.rikisaepulloh.appsikampus.pojos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "mahasiswa", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idMahasiswa")
public class Mahasiswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long idMahasiswa;
    @Column(columnDefinition = "varchar(50)")
    private String namaMahasiswa;
    @Column(columnDefinition = "char(13)", unique = true)
    private String nim;
    @Column(columnDefinition = "char(7)")
    private String jnsKelamin;
    @Column(columnDefinition = "varchar(50)")
    private String tmpLahir;
    @Column(columnDefinition = "date")
    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date tglLahir;

    public long getIdMahasiswa() {
        return idMahasiswa;
    }

    public void setIdMahasiswa(long idMahasiswa) {
        this.idMahasiswa = idMahasiswa;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getJnsKelamin() {
        return jnsKelamin;
    }

    public void setJnsKelamin(String jnsKelamin) {
        this.jnsKelamin = jnsKelamin;
    }

    public String getTmpLahir() {
        return tmpLahir;
    }

    public void setTmpLahir(String tmpLahir) {
        this.tmpLahir = tmpLahir;
    }

    public Date getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Date tglLahir) {
        this.tglLahir = tglLahir;
    }

    public List<Kelas> getKelass() {
        return kelass;
    }

    public void setKelass(List<Kelas> kelass) {
        this.kelass = kelass;
    }

    @OneToMany(mappedBy = "mahasiswa", cascade = CascadeType.ALL, targetEntity = Kelas.class)
    private List<Kelas> kelass;

}
