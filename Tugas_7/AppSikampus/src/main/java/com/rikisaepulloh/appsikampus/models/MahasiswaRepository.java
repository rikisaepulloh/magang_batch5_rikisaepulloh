package com.rikisaepulloh.appsikampus.models;

import com.rikisaepulloh.appsikampus.pojos.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Long>, JpaSpecificationExecutor<Mahasiswa> {

}
