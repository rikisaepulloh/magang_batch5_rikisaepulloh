package com.rikisaepulloh.appsikampus.services;

import com.rikisaepulloh.appsikampus.pojos.Kelas;

import java.util.List;
import java.util.Map;

public interface KelasService extends BaseService<Kelas> {

    Map<String, Object> saveData(List<Kelas> par);

}
