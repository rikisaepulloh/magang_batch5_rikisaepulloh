package com.rikisaepulloh.appsikampus.services;

import com.rikisaepulloh.appsikampus.pojos.Dosen;

import java.util.Map;

public interface DosenService extends BaseService<Dosen> {

    Map<String, Object> saveDataMatakuliah(Dosen par);
}
