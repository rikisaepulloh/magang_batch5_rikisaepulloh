package com.rikisaepulloh.appsikampus.models;

import com.rikisaepulloh.appsikampus.pojos.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface KelasRepository extends JpaRepository<Kelas, Long>, JpaSpecificationExecutor<Kelas> {

}
