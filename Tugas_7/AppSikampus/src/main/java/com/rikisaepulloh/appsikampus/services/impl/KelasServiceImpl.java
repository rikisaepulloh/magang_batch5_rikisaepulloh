package com.rikisaepulloh.appsikampus.services.impl;

import com.rikisaepulloh.appsikampus.models.KelasRepository;
import com.rikisaepulloh.appsikampus.pojos.Kelas;
import com.rikisaepulloh.appsikampus.services.KelasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Service
public class KelasServiceImpl implements KelasService {

    @Autowired
    private KelasRepository kelasRepository;

    @Override
    public Page<Kelas> findDataAll(Map<String, Object> par) {
        String parSort = par.get("sort") == null ? "idKelas" : par.get("sort").toString();
        String order = par.get("order") == null ? "asc" : par.get("order").toString();
        int pg = par.get("page") == null ? 0 : Integer.parseInt(par.get("page").toString()) - 1;
        int size = par.get("size") == null ? 10 : Integer.parseInt(par.get("size").toString());
        Sort sort = Sort.by(parSort);
        if (order.equals("asc")) {
            sort = sort.ascending();
        } else {
            sort = sort.descending();
        }
        Pageable pageable = PageRequest.of(pg, size, sort);
        Page<Kelas> page = null;
        if (par.get("keyword") == null) {
            page = kelasRepository.findAll(pageable);
        } else {
            pageable = PageRequest.of(pg, size, sort);
            page = kelasRepository.findAll(new Specification<Kelas>() {
                @Override
                public Predicate toPredicate(Root<Kelas> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(criteriaBuilder.like(root.get("nmKelas"), "%" + par.get("keyword").toString() + "%"));
                    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
            }, pageable);
        }
        return page;
    }

    @Override
    public Optional<Kelas> findDataById(long id) {
        return kelasRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Kelas par) {
        Map<String, Object> map = new HashMap<>();
        Kelas kelas = kelasRepository.save(par);
        if (kelas.getMahasiswaMatakuliahIdentity().getIdMahasiswa() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> saveData(List<Kelas> par) {
        Map<String, Object> map = new HashMap<>();
        List<Kelas> kelas = kelasRepository.saveAll(par);
        if (kelas.get(kelas.size() - 1).getMahasiswaMatakuliahIdentity().getIdMahasiswa() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Kelas par) {
        Map<String, Object> map = new HashMap<>();
        kelasRepository.delete(par);
        long cn = kelasRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}
