package com.rikisaepulloh.appstrukturorganisasi;

import javax.persistence.*;

@Entity
@Table(name = "data_employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nama;
    private String jabatan;
    private String atasanId;
    private String company;
    public long getId() {

        return id;
    }
    public void setId(long id) {

        this.id = id;
    }
    public String getNama() {

        return nama;
    }
    public void setNama(String nama) {

        this.nama = nama;
    }
    public String getCompany() {

        return company;
    }
    public void setCompany(String company) {

        this.company = company;
    }
    public String getAtasanId() {
        return atasanId;
    }
    public void setAtasanId(String atasanId) {
        this.atasanId = atasanId;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", jabatan='" + jabatan + '\'' +
                ", atasanId='" + atasanId + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
