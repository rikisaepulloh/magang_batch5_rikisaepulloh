package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List getAllEmployee(){
        List employee = new ArrayList<>();
        employeeRepository.findAll().forEach(employee :: add);
        return employee;
    }
    public void addEmployee(Employee employee){
        employeeRepository.save(employee);
    }
    public Optional<Employee> getEmployee(long id){
        return employeeRepository.findById(id);
    }
    public void updateEmployee(long id, Employee employee){
        employeeRepository.save(employee);
    }
    public void deleteEmployee(long id){
        employeeRepository.deleteById(id);
    }
}