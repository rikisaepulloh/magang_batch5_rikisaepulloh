package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1")
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public List getAllEmployee(){
        return service.getAllEmployee();
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public Optional<Employee> getEmployee(@PathVariable("id") long id){
        return service.getEmployee(id);
    }

    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public void addEmployee(@RequestBody Employee employee){
        service.addEmployee(employee);
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.PUT)
    public void updateEmployee(@RequestBody Employee employee, @PathVariable("id") long id){
        service.updateEmployee(id, employee);
    }
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable("id") long id){
        service.deleteEmployee(id);
    }
}
