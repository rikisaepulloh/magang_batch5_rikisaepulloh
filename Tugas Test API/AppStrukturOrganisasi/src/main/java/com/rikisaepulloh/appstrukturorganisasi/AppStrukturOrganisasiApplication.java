package com.rikisaepulloh.appstrukturorganisasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppStrukturOrganisasiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppStrukturOrganisasiApplication.class, args);
    }

}
