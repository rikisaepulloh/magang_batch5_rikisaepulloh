-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Apr 2021 pada 17.06
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `departemen`
--

CREATE TABLE `departemen` (
  `id` int(100) PRIMARY key,
  `nama` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `departemen`
-- 

INSERT INTO `departemen` (`id`, `nama`) VALUES
(1, 'Manajemen'),
(2, 'Pengembangan Bisnis'),
(3, 'Teknisi'),
(4, 'Analisis');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(100) PRIMARY key,
  `nama` varchar(256) NOT NULL,
  `jenis_kelamin` enum('P','L') NOT NULL,
  `status` enum('Menikah','Belum') NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `departemen` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES
(1, 'Rizky Saputra', 'L', 'Menikah', '1980-10-11', '2011-01-01', 1),
(2, 'Farhan Reza', 'L', 'Belum', '1989-11-01', '1989-01-01', 1),
(3, 'Riyando Adi', 'L', 'Menikah', '1977-01-25', '2011-01-01', 1),
(4, 'Diego Manuel', 'L', 'Menikah', '1983-02-22', '2012-09-04', 2),
(5, 'Satya Laksana', 'L', 'Menikah', '1981-01-12', '2011-03-19', 2),
(6, 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-06-15', 2),
(7, 'Putri Persada', 'P', 'Menikah', '1988-01-30', '2013-04-14', 2),
(8, 'Alma Safira', 'P', 'Menikah', '1991-05-18', '2013-09-28', 3),
(9, 'Haqi Hafiz', 'L', 'Belum', '1995-09-19', '2015-03-09', 3),
(10, 'Abi Isyawara', 'L', 'Belum', '1989-11-01', '2011-01-01', 3),
(11, 'Maman Kresna', 'L', 'Belum', '1993-08-21', '2012-09-15', 3),
(12, 'Nadia Aulia', 'P', 'Belum', '1989-10-07', '2012-05-07', 4),
(13, 'Mutiara Rezki', 'P', 'Menikah', '1988-03-23', '2013-05-21', 4),
(14, 'Dani Setiawan', 'L', 'Belum', '1986-02-11', '2014-11-30', 4),
(15, 'Budi Putra', 'L', 'Belum', '1995-10-23', '2015-12-03', 4);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

update karyawan set status="Menikah" where id = "2";

update karyawan set status="Menikah" where id = "12";

update karyawan set status="Menikah" where id = "10";

update karyawan set status="Menikah" where id = "14";

delete from karyawan where id = "4";

delete from karyawan where id = "8";

delete from karyawan where id = "11";

select * from karyawan where departemen > 1; 


