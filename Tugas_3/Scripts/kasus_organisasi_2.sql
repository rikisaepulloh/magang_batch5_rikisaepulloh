create table `employee`(
`id` int(100) primary key,
`nama` varchar(256) not null,
`atasan_id` int(100),
`company_id` int(100) not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

create table `company`(
`id` int(100) primary key,
`nama` varchar(256) not null,
`alamat` varchar(256) not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `employee` (`id`, `nama`,`atasan_id`,`company_id`) VALUES
(1, 'Pak Budi',0,1),
(2, 'Pak Tono',1,1),
(3, 'Pak Totok',1,1),
(4, 'Bu Sinta',2,1),
(5, 'Bu Novi',3,1),
(6, 'Andre',4,1),
(7, 'Dono',4,1),
(8, 'Ismir',5,1),
(9, 'Anto',5,1);

INSERT INTO `company` (`id`, `nama`,`alamat`) VALUES
(1, 'PT JAVAN','Sleman'),
(2, 'PT Dicoding','Bandung');

select * from employee where atasan_id = 0;

select * from employee where atasan_id > 4;

select * from employee where atasan_id = 1;

select * from employee where atasan_id <= 3 and atasan_id > 1;

select * from employee where atasan_id >= 1 ;

select * from employee where atasan_id = 4;